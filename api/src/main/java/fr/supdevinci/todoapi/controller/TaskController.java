package fr.supdevinci.todoapi.controller;

import fr.supdevinci.todoapi.model.TaskDAO;
import fr.supdevinci.todoapi.repository.TaskRepository;
import fr.supdevinci.todoapi.service.TaskService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("task")
public class TaskController {

    private final TaskService service;
    private final TaskRepository repository;

    public TaskController(TaskService service, TaskRepository repository) {
        this.service = service;
        this.repository = repository;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<TaskDAO> getTasksOfUser() {
        return service.getAllTaskOfUser();
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public TaskDAO addTask(@RequestBody TaskDAO task) {
        return service.addTask(task);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteById(@PathVariable("id") int id) {
        service.deleteTask(id);
    }

    @PutMapping("/{id}")
    TaskDAO patchById(@RequestBody TaskDAO newTask, @PathVariable int id) {
        return service.changeTaskStatus(newTask, id);
    }

}