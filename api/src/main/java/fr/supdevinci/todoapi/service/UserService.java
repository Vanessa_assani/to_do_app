package fr.supdevinci.todoapi.service;

import fr.supdevinci.todoapi.model.UserDao;
import fr.supdevinci.todoapi.repository.UserRepository;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Data
@Service
public class UserService {

    private final UserRepository repository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.repository = userRepository;
    }

    public UserDao get(String login) {
        return repository.findByLogin(login);
    }

    public UserDao save(UserDao user) {
        return repository.save(user);
    }

}
