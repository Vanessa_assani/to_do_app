package fr.supdevinci.todoapi.service;

import fr.supdevinci.todoapi.model.TaskDAO;
import fr.supdevinci.todoapi.repository.TaskRepository;
import fr.supdevinci.todoapi.repository.UserRepository;
import lombok.Data;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.List;

@Data
@Service
public class TaskService {

    private final UserRepository userRepository;
    private final TaskRepository taskRepository;

    public TaskService(UserRepository userRepository, TaskRepository taskRepository) {
        this.userRepository = userRepository;
        this.taskRepository = taskRepository;
    }

    public List<TaskDAO> getAllTaskOfUser() {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String login = userDetails.getUsername();
        return userRepository.findByLogin(login).getTasks();
    }

    public void deleteTask(int id) {
        if(!taskRepository.existsById(id)) return;
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String login = userDetails.getUsername();
        TaskDAO task = taskRepository.getById(id);
        if(!task.getUser().getLogin().equals(login)) return;
        taskRepository.deleteById(id);
    }

    public TaskDAO addTask(TaskDAO task) {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String login = userDetails.getUsername();
        task.setUser(userRepository.findByLogin(login));
        return taskRepository.save(task);
    }

    public TaskDAO changeTaskStatus(TaskDAO newTask, int id) {
        return taskRepository.findById(id)
                .map(task -> {
                    if(!((UserDetails)(SecurityContextHolder.getContext().
                            getAuthentication().getPrincipal())).getUsername()
                            .equals(task.getUser().getLogin())) return task;
                    if(newTask.getLabel() != null) task.setLabel(newTask.getLabel());
                    if(newTask.getStatus() != null) task.setStatus(newTask.getStatus());
                    return taskRepository.save(task);
                })
                .orElseGet(() -> {
                    return taskRepository.save(newTask);
                });
    }

}
