package fr.supdevinci.todoapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "task")
@Setter
@Getter
public class TaskDAO {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;

    private String label;

    private Boolean status;

    @ManyToOne
    @JoinColumn(name="user_login")
    @JsonIgnore
    private UserDao user;

}
