package fr.supdevinci.todoapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "user")
@Setter
@Getter
public class UserDao {

    @Id
    private String login;

    private String password;

    @OneToMany(mappedBy="user")
    private List<TaskDAO> tasks = new ArrayList<>();

}
