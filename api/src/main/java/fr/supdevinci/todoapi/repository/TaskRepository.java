package fr.supdevinci.todoapi.repository;

import fr.supdevinci.todoapi.model.TaskDAO;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskRepository extends JpaRepository<TaskDAO, Integer> {
}