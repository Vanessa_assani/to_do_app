package fr.supdevinci.todoapi.repository;

import fr.supdevinci.todoapi.model.UserDao;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserDao, Integer> {

    UserDao findByLogin(String login);

}