USE todo_app;

CREATE TABLE IF NOT EXISTS hibernate_sequence
(
    next_val bigint null
)

ENGINE = MyISAM;

CREATE TABLE IF NOT EXISTS user
(
    login varchar(255) not null primary key,
    password varchar(255) not null
);

CREATE TABLE IF NOT EXISTS task
(
    id int auto_increment primary key,
    label varchar(255) not null,
    status tinyint(1) default 0 not null,
    user_login varchar(255) not null,
    constraint fk_user foreign key (user_login) references user (login)
);
