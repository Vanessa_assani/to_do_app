# To do application

TO_DO_APP est une application de gestion de tâche d'un utilisateur

Fonctionnalités:
-Ajout d'un utilisateur:
L'application a une page d'incription pour un user et une page de connexion sinon.

-Ajout d'une tâche pour un utilisateur donné:
Une fois authentifié, l'utilisateur a accès à une page qui lui permet d'ajouter ses différentes tâches.

-Affichage des tâches:
Il a également la possibilité de lister ses différentes tâches.

-Modifier le status d'une tâche:
Après vérification de l'existence de la tâche dans la bd, l'user peut changer son status.

-Suppression d'une tâche
Supprimer une tâche déjà créée.



Organisation du travail :
Front de l'appli :  Angular, HTML/CSS, Javascript
back de l'appli :   Firebase

## API
Cette API fonctionne sur le framework Springboot et utilise une base de données MySQL.
Elle permet aux utilisateurs de :
- Créer un compte
- Se connecter à leur compte
- Supprimer, ajouter, lister leurs tâches

### Modèle REST
```
POST    /authenticate       # Permet de se connècte a un compte.
                            # Demande 'login' et 'password'
                            # Renvoie un token d'identification
                            
POST    /register           # Créer un compte
                            # Demande 'login' et 'password'
                            
GET     /task               # Renvoie les tâches de l'utilistateur
                            
POST    /task               # Créer une nouvelle tâche
                            # Demande 'label' et 'status'
                            # Renvoie la tâche créer

PUT     /task               # Modifier une tâche existante
                            # Renvoie la tâche modifier
```

### Lancer les testes
```
> cd api
> ./mvnw test
```

### Build l'API
```
> cd api
> ./mvnw clean package
```

### Déployer l'API avec Docker
**INFO :** Il est nécessaire de build l'API avant de la déployer !
```
> docker-compose up
```