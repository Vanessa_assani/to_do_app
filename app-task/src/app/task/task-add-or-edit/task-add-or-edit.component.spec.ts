import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskAddOrEditComponent } from './task-add-or-edit.component';

describe('TaskAddOrEditComponent', () => {
  let component: TaskAddOrEditComponent;
  let fixture: ComponentFixture<TaskAddOrEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TaskAddOrEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskAddOrEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
