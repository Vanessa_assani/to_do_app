import { User } from './user.model';
export interface Task {
  id: number;
  name: string;
  status: boolean;
  user_id: User;
}
