import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserEditOrAddComponent } from './user-edit-or-add.component';

describe('UserEditOrAddComponent', () => {
  let component: UserEditOrAddComponent;
  let fixture: ComponentFixture<UserEditOrAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserEditOrAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserEditOrAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
